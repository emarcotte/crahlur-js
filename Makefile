deps := $(wildcard libs/**/*.js )

all: deps
	coffee -c --output build src

devserver:
	make -j 2 serve development

serve:
	python -m SimpleHTTPServer 8080

development:
	coffee -cw --output build src

clean:
	rm -rf build

deps: $(deps)
	mkdir -p build
	cp -R libs/* build


Crahlur
-------

Crahlur is supposed to be a dungeon crawler.

Dependencies
------------

This project requires several libraries to run. With the exception of
CoffeeScript, all the listed dependencies are included in the source tree.

* RequireJS - http://requirejs.org/
* three.js - http://threejs.org/
* CoffeeScript - http://coffeescript.org/

Build
-----

On systems with coffescript and python installed you should able to get a
webserver running crahlur at http://localhost:8080 with this command:

	make devserver

Alternatively, you can use `make` to simply build the files and dependencies
into the `build/` directory. Note, there are several other targets provided in
the `Makefile` to ease development.

If you are unable to use make, python, or coffeescript from the commandline,
you will have to find some other mechanism for compiling the files in `src/`
into `build/` and serving them.

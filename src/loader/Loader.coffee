define [ 'three/three' ], ( _three ) ->
	class Loader
		constructor: () ->
			@assets = {}

		load: ( assets ) ->
			loaders =
				img: ( asset, name ) =>
					THREE.ImageUtils.loadTexture(
						name,
						undefined,
						( image ) => @assets[name] = image,
						( error ) -> console.error( error )
					)
				audio: ( asset, name ) =>
					loader = @
					settings = {}
					settings.volume = asset.volume if asset.volume
					settings.buffer = asset.buffer if asset.buffer
					settings.urls = asset.urls
					settings.onload = ( ) ->
						loader.assets[name] = @
						asset.callback( @ ) if asset.callback
					audio = new Howl( settings )
				model: ( asset, name ) =>
					modelLoader = new THREE.JSONLoader()
					modelLoader.load( name, ( geo, mat ) =>
						@assets[name] =
							geometry: geo
							materials: mat
						return
					)

			for asset in assets
				loader = loaders[asset.type]
				loader( asset, asset.name )

		done: ( assets ) ->
			for asset in assets
				if ! @assets[asset.name]
					return false
			return true

		get: ( name ) ->
			value = @assets[name]
			if ! value
				throw "Unknown asset #{name}"
			return value


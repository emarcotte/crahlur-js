define(
	[ 'state/State', 'text/Renderer', 'text/Font' ],
	( State, TextRenderer, Font ) ->
		class AnimationController
			constructor: () ->
				@animations = []

			update: ( delta ) ->
				for animation in @animations
					animation.update( delta )

		class Game extends State
			constructor: () ->
				super
				@assets = [
					{ name: 'assets/goblin1/goblin1.js', type: 'model' }
				]

				@animationController = new AnimationController()
				@controllers.push( @animationController )

			resize: ( width, height ) ->
				@camera3d.aspect = width / height
				@camera3d.updateProjectionMatrix()
				@camera2d.right = width
				@camera2d.bottom = height
				@camera2d.updateProjectionMatrix()
				#@uiController.resize( width, height )

			getAssets: () ->
				return @assets

			onStart: ( game ) ->
				@game = game

				game.renderer.setClearColor( 0x2e2e2e, 1 )

				@scene3d = new THREE.Scene()
				@scene2d = new THREE.Scene()
				game.renderer.autoClear = false

				@camera2d = new THREE.OrthographicCamera(
					0,
					window.innerWidth,
					0,
					window.innerHeight,
				)
				@camera2d.position.z = 100
				@camera3d = new THREE.PerspectiveCamera(
					60,
					window.innerWidth / window.innerHeight,
					1,
					10000
				)
				@camera3d.position.y = 10
				@camera3d.position.z = 10
				@camera3d.lookAt(new THREE.Vector3())

				model = game.loader.get('assets/goblin1/goblin1.js')
				for material in model.materials
					material.skinning = true

				@goblin = new THREE.SkinnedMesh(
					model.geometry,
					new THREE.MeshFaceMaterial( model.materials ),
					false
				)
				@scene3d.add( @goblin )
				THREE.AnimationHandler.add( model.geometry.animation )
				animation = new THREE.Animation( @goblin, "Action" )
				animation.play()
				@animationController.animations.push( animation )

				plane = new THREE.Mesh(
					new THREE.PlaneGeometry( 3, 3 ),
					new THREE.MeshNormalMaterial()
				)
				plane.rotateOnAxis( new THREE.Vector3( -1, 0, 0 ), 90 )
				@scene3d.add( plane )

				font = new Font({
					font: "sans-serif",
					bg: "rgba(200,200,200,0.5)",
					stroke: 'rgb(0,0,0)',
					size: 40,
				})
				text = TextRenderer.render( font, "t的☃ ЯЦѕѕіа" )
				@scene2d.add( text )
				game.input.motionEvent.push( ( e ) ->
					text.position.set( e.clientX - 16 * 4, e.clientY - 30, 0 )
				)

			onStop: ( game ) ->
				game.renderer.autoClear = true

			render: ( game ) ->
				game.renderer.clear()
				game.renderer.render( @scene3d, @camera3d )
				game.renderer.render( @scene2d, @camera2d )
)

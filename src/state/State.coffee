define [ ], ( ) ->
	class State
		constructor: () ->
			@controllers = []

		# update everything then render.
		renderFrame: ( game, delta ) ->
			for controller in @controllers
				controller.update( delta )

			@render( game )

		getAssets: () ->
			return []

		onStart: ( game ) ->

		onStop: ( game ) ->

		render: ( game ) ->

		resize: ( width, height ) ->



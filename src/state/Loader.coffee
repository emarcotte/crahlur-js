define [ 'state/State' ], ( State, GameState ) ->
	class Loader extends State
		constructor: ( @nextState ) ->
			super
			@assets = @nextState.getAssets()

		getAssets: () ->
			return @assets

		resize: ( width, height ) ->
			@camera.aspect = width / height
			@camera.updateProjectionMatrix()
			#@uiController.resize( width, height )

		onStart: ( game ) ->
			@scene = new THREE.Scene()

			game.renderer.setClearColor( 0x2e2e2e, 1 )
			game.renderer.autoClear = false
			game.loader.load( @nextState.getAssets() )

			@camera = new THREE.PerspectiveCamera(
				60,
				window.innerWidth / window.innerHeight,
				1,
				10000
			)
			@camera.position.y = -200
			@camera.position.z = 200
			@camera.lookAt(new THREE.Vector3())

			#@bgController = new BackgroundController(@scene)
			#@goController = new GameObjectController(game)
			#@uiController = new UIController(game)

			#@controllers.push(
			#	@bgController,
			#	@goController,
			#	@uiController
			#)

		onStop: ( game ) ->
			game.renderer.autoClear = true

		render: ( game ) ->
			if game.loader.done( @getAssets() )
				game.operations.push( () => game.setState( @nextState ) )

			game.renderer.clear()
			game.renderer.render( @scene, @camera )
			#game.renderer.render( @uiController.scene, @uiController.camera )



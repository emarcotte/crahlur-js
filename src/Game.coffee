define(
	[ 'three/three', 'state/Loader', 'state/Game', 'loader/Loader' ],
	( _three, LoaderState, GameState, AssetLoader ) ->
		class InputManager
			constructor: ( @element ) ->
				@motionEvent = []
				@clickEvent = []
				@keyUpEvent = []
				@keyDownEvent = []
				@keys = {}

				# Focus the DOM node that is doing all the input capturing.
				@element.tabIndex = 1
				@element.focus()

				# Add global event handlers.
				@element.addEventListener( 'mousemove', @mouseMove.bind( @ ) )
				@element.addEventListener( 'keydown', @keyDown.bind( @ ) )
				@element.addEventListener( 'keyup', @keyUp.bind( @ ) )
				@element.addEventListener( 'click', @click.bind( @ ) )

			click: ( e ) ->

			mouseMove: ( e ) ->
				e.preventDefault()
				for motion in @motionEvent
					motion( e )
				return

			keyDown: ( e ) ->

			keyUp: ( e ) ->

		class Game
			constructor: () ->
				@renderer = new THREE.WebGLRenderer({
					antialias: true
				})
				@callback = @update.bind( @ )

				# Load URL settings.
				@settings = {}
				window.location.href.replace(
					/[?&]+([^=&]+)=([^&]*)/gi,
					( match, key, value ) => @settings[key] = value
				)

				@operations = []
				document.body.insertBefore(
					@renderer.domElement,
					document.body.firstChild
				)

				@input = new InputManager( @renderer.domElement )

				window.onresize = @resize.bind( @ )

				@loader = new AssetLoader()
				@setState( new LoaderState( new GameState() ) )

				@lastFrame = Date.now()
				@resize()
				@update()

			setState: ( state ) ->
				if @state
					@state.onStop( @ )

				@state = state
				@state.onStart( @ )

			resize: ( event ) ->
				width = window.innerWidth
				height = window.innerHeight
				@state.resize( width, height )
				@renderer.setSize(width, height)

			update: () ->
				while( op = @operations.pop() )
					op( @ )

				delta = Date.now() - @lastFrame
				@lastFrame = Date.now()

				@state.renderFrame( @, delta )

				# and then request another frame draw
				requestAnimationFrame( @callback )

		# Fire it up!
		game = new Game()
)

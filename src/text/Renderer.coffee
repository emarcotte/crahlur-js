define(
	[ 'three/three' ],
	( _three ) ->
		render: ( font, text ) ->
			currentChar = 0
			x = 0
			y = 0

			textures = {}

			while currentChar < text.length
				character = text.charAt( currentChar )
				code = character.charCodeAt( 0 )
				metrics = font.getMetrics( code )
				texture = font.getCodeTexture( code )

				if ! textures[texture.id]
					textures[texture.id] = {
						currentFace: 0,
						geometry: new THREE.Geometry(),
						texture: texture,
					}
					textures[texture.id].geometry.faceVertexUvs = [[]]

				geometry = textures[texture.id].geometry

				right = x + metrics.uw
				right = ~~right
				geometry.vertices.push( new THREE.Vector3( x, 0, 0 ) )
				geometry.vertices.push( new THREE.Vector3( x, font.settings.size, 0 ) )
				geometry.vertices.push( new THREE.Vector3( right, 0, 0 ) )
				geometry.vertices.push( new THREE.Vector3( right, font.settings.size, 0 ) )

				firstVertex = textures[texture.id].currentFace * 4

				geometry.faces.push(
					new THREE.Face4(
						firstVertex,
						firstVertex + 1,
						firstVertex + 3,
						firstVertex + 2,
					)
				)

				geometry.faceVertexUvs[0].push( [
					new THREE.Vector2( metrics.u, metrics.v ),
					new THREE.Vector2( metrics.u, metrics.v - metrics.h ),
					new THREE.Vector2( metrics.u + metrics.w, metrics.v - metrics.h),
					new THREE.Vector2( metrics.u + metrics.w, metrics.v ),
				])

				textures[texture.id].currentFace++
				currentChar++

				x = right

			object = new THREE.Object3D()
			for id, hash of textures
				geometry = hash.geometry
				geometry.computeCentroids()
				geometry.computeFaceNormals()
				material = new THREE.MeshBasicMaterial({
					map: hash.texture,
					transparent: true,
				})
				material.blending = THREE.CustomBlending
				material.blendSrc = THREE.SrcAlphaFactor
				material.blendDst = THREE.OneMinusSrcAlphaFactor
				material.blendEquation = THREE.AddEquation

				object.add( new THREE.Mesh( geometry, material ) )

			return object
)

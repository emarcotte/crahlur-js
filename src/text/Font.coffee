define(
	[ 'three/three' ],
	( _three ) ->
		class Font
			textureSize = 1024

			constructor: ( @settings ) ->
				@settings ||= {}
				@settings.font ||= 'serif'
				@settings.size ||= 16
				@settings.fg ||= 'white'
				@fontString = "#{@settings.size}px #{@settings.font}"
				@characters = {}
				@textures = []
				@sets = {}
				@charactersPerSet = ~~( textureSize / ( @settings.size + 5 ) )
				@charactersPerSet = @charactersPerSet * @charactersPerSet

			# [ 0x20 .. 0x7E ] # simple ascii
			# [ 0x4E00 .. 0x9FFF ] Simplified chinese common range
			getSetNumber: ( code ) ->
				return Math.floor( code / @charactersPerSet )

			getSetRange: ( set ) ->
				return {
					start: @charactersPerSet * set,
					end: @charactersPerSet * (set + 1) - 1
				}

			getCodeTexture: ( code ) ->
				set = @getSetNumber( code )
				return @textures[ @sets[ set ] ]

			getSetTexture: ( set ) ->
				return @textures[ @sets[set] ]

			getMetrics: ( code ) ->
				if ! @characters[ code ]
					@renderSet( code )

				return @characters[ code ]

			renderSet: ( code ) ->
				set = @getSetNumber( code )
				range = @getSetRange( set )
				console.log( "Making text texture for codes: #{ range.start } #{ range.end }" )
				currentCode = range.start

				canvas = document.createElement('canvas')
				canvas.width = canvas.height = textureSize
				context = canvas.getContext('2d')
				context.font = @fontString
				size = @settings.size

				# Fill the bg color
				if @settings.bg
					context.fillStyle = @settings.bg
					context.fillRect( 0, 0, textureSize, textureSize )

				# Set up font styling
				if @settings.stroke
					context.strokeStyle = @settings.stroke

				context.fillStyle = @settings.fg
				context.textBaseline = 'top'

				# Keep track of position, fill top left to bottom right.
				x = 0
				y = 0
				textureId = @textures.length
				@sets[ set ] = textureId

				while currentCode < range.end
					char = String.fromCharCode( currentCode )
					metrics = context.measureText( char )

					if x + metrics.width > canvas.width
						x = 0
						y += size + 2

					if y + size > canvas.height
						throw "Character range #{ range.start }-#{ range.end } doesnt fit in texture"

					@characters[ currentCode ] = {
						u: x / textureSize,
						v: 1 - ( y / textureSize ),
						h: size / textureSize,
						uw: metrics.width,
						w: metrics.width / textureSize,
					}

					context.fillText( char, x, y )
					if @settings.stroke
						context.strokeText( char, x, y )

					x += ~~( metrics.width + 2 )
					currentCode++

				texture = new THREE.Texture( canvas )
				texture.needsUpdate = true
				@textures.push( texture )
)
